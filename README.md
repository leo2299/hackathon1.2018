                                                                HACKATHON  1º SEMESTRE 2018
                                                                
    Descrição:
    
    - Versão piloto do arquivo Hackathon1.2018.
    
    - Partindo do princípio que o COP-BH já sabe a localização de um chamado, cada equipe contará com um radio digital integrado com um GPS, desse modo saberemos a localização exata de cada equipe.
      Todavia, teremos que fazer com que o chamado seja direcionado automaticamente para a "equipe" mais próxima.
      Trazendo assim uma maior agilidade do atendimento e evitando que outras "equipes" se desloquem sem necessidade.
     
    
    Pré-requisitos:
    
    - Foi utilizado para a criação e funcionamento do arquivo:
        .Sublime
        .Windows 10
        .Note.js
        .API Google
        
    Guia de instalção:
    
    - Realizar o funcionamento do programa através da abertura de uma página web que será enviada dentro de uma pasta.
    
    Licença:
    
    - Como é uma versão piloto, então o software estará com uma licença MIT
    
    Autoria e contribuições:
    
    - Alunos: Leonardo do Carmo Almeida11511307, Thiago Felipe Pinto de Oliveira11516023 e Hugo Vinícius Freitas Losqui11321083.
    
    Agradecimentos:
    
    - UNIBH (em especial à coordenadora Samara) e  profissionais da AVSystemGEO em parceria com a Prodabel que nos proporcionaram esta oportunidade única.
    
    